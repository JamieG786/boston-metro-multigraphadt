package CS308.Group1.BostonMetro;

import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void message(){
        /*
        No tests exists for this class as the main method does not
        use it's parameters, returns no values, and there is no internal
        'state' stored. It is impossible to test the main method as is.

        This file exists to show that consideration was given to all areas
        of testing.
         */
    }

}