package CS308.Group1.BostonMetro;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StationTest {

    @Test
    void createWithNullName(){
        assertThrows(IllegalArgumentException.class, () -> new Station(null, 3));
    }

    @Test
    void getName() {
        Station station = new Station("x", 0);
        assertEquals("x", station.getName());
    }

    @Test
    void getId() {
        Station station = new Station("", 999);
        assertEquals(999, station.getId());
    }
}