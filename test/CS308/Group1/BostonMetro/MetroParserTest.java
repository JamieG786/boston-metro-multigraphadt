package CS308.Group1.BostonMetro;

import CS308.Group1.Graph.LabelledMultiGraph;
import CS308.Group1.Graph.StickyShortestLabelledEdgePathFinder;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class MetroParserTest {
    @Test
    void nodesNotNullTest() throws IOException {
        ArrayList<Station> list = new ArrayList<>();
        LabelledMultiGraph<Station, String>graph = LabelledMultiGraph.newInstance();
        MetroMapParser parser = new MetroMapParser("bostonmetro.txt");
        assertEquals(graph.getNodes(), list);
        parser.generateGraphFromFile(graph);
        assertNotEquals(graph.getNodes(), list);
    }
    @Test
    void extraWhiteSpaceTest() throws IOException {
        ArrayList<Station> list = new ArrayList<>();
        LabelledMultiGraph<Station, String>graph = LabelledMultiGraph.newInstance();
        MetroMapParser whiteSpaceParser = new MetroMapParser("extrawhitespace.txt");
        assertEquals(graph.getNodes(), list);
        whiteSpaceParser.generateGraphFromFile(graph);
        assertNotEquals(graph.getNodes(), list);
    }
    @Test
    void grammarErrorsTest() throws IOException {
        ArrayList<Station> list = new ArrayList<>();
        LabelledMultiGraph<Station, String>graph = LabelledMultiGraph.newInstance();
        MetroMapParser whiteSpaceParser = new MetroMapParser("grammarerrors.txt");
        assertEquals(graph.getNodes(), list);
        whiteSpaceParser.generateGraphFromFile(graph);
        assertNotEquals(graph.getNodes(), list);
    }
    @Test
    void fullTestExtraWhiteSpace() throws IOException {
        //Regular graph setup
        LabelledMultiGraph<Station, String> metroRoute = LabelledMultiGraph.newInstance();
        MetroMapParser parser = new MetroMapParser("bostonmetro.txt");
        parser.generateGraphFromFile(metroRoute);
        MetroRoute firstRoute = new MetroRoute(new StickyShortestLabelledEdgePathFinder<>(metroRoute));
        
        List<Station> firstStations = parser.getStations();
        Station firstOrigin = firstStations.get(5);
        Station firstDestination = firstStations.get(10);
        firstRoute.newOrigin(firstOrigin);
        firstRoute.newDestination(firstDestination);

        //Extra white space
        LabelledMultiGraph<Station, String> metro = LabelledMultiGraph.newInstance();
        MetroMapParser whiteSpaceParser = new MetroMapParser("extrawhitespace.txt");
        whiteSpaceParser.generateGraphFromFile(metro);
        MetroRoute route = new MetroRoute(new StickyShortestLabelledEdgePathFinder<>(metro));
        
        List<Station> stations = whiteSpaceParser.getStations();
        Station origin = stations.get(5);
        Station destination = stations.get(10);
        route.newOrigin(origin);
        route.newDestination(destination);
        assertEquals(firstRoute.condensedDirections(), route.condensedDirections());
    }
    @Test
    void grammarErrors() throws IOException {
        //Regular graph setup
        LabelledMultiGraph<Station, String> metroRoute = LabelledMultiGraph.newInstance();
        MetroMapParser parser = new MetroMapParser("bostonmetro.txt");
        parser.generateGraphFromFile(metroRoute);
        MetroRoute firstRoute = new MetroRoute(new StickyShortestLabelledEdgePathFinder<>(metroRoute));
        
        List<Station> firstStations = parser.getStations();
        Station firstOrigin = firstStations.get(5);
        Station firstDestination = firstStations.get(10);
        firstRoute.newOrigin(firstOrigin);
        firstRoute.newDestination(firstDestination);

        //Extra white space
        LabelledMultiGraph<Station, String> metro = LabelledMultiGraph.newInstance();
        MetroMapParser whiteSpaceParser = new MetroMapParser("grammarerrors.txt");
        whiteSpaceParser.generateGraphFromFile(metro);
        MetroRoute route = new MetroRoute(new StickyShortestLabelledEdgePathFinder<>(metro));
        
        List<Station> stations = whiteSpaceParser.getStations();
        Station origin = stations.get(5);
        Station destination = stations.get(10);
        route.newOrigin(origin);
        route.newDestination(destination);
        assertEquals(firstRoute.condensedDirections(), route.condensedDirections());
    }

}



