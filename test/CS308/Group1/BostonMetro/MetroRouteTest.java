package CS308.Group1.BostonMetro;

import CS308.Group1.Graph.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MetroRouteTest {
    @Test
    void fromPathFinderTest() {
        System.out.println("Building graph...");
        LabelledMultiGraph<Station, String> graph = LabelledMultiGraph.newInstance();
        Station n1 = new Station("N1", 1);
        Station n2 = new Station("N2", 2);
        Station n3 = new Station("N3", 3);
        Station n4 = new Station("N4", 4);
        graph.addNode(n1);
        graph.addNode(n2);
        graph.addNode(n3);
        graph.addNode(n4);
        try {
            graph.addEdge(LabelledEdge.newInstance(n1, n2, "between1n2"));
            graph.addEdge(LabelledEdge.newInstance(n2, n3, "between2n3"));
            graph.addEdge(LabelledEdge.newInstance(n1, n3, "between1n3"));
            graph.addEdge(LabelledEdge.newInstance(n1, n4, "between1n4"));
            graph.addEdge(LabelledEdge.newInstance(n4, n3, "between4n3"));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }

        System.out.println("Build route...");
        MetroRoute sr = new MetroRoute(
                n1, n3, new StickyShortestLabelledEdgePathFinder<>(graph)
        );

        System.out.println("Start assertions...");
        assertEquals(sr.getStations().get(0).from().getName(), "N1");
        assertEquals(sr.getStations().get(0).to().getName(), "N3");
        assertEquals(sr.getStations().get(0).getLabel(), "between1n3");
    }

    @Test
    void changeOriginFunctionTest() {
        // Set up graph
        System.out.println("Building graph...");
        LabelledMultiGraph<Station, String> graph = LabelledMultiGraph.newInstance();
        Station n1 = new Station("N1", 1);
        Station n2 = new Station("N2", 2);
        Station n3 = new Station("N3", 3);
        Station n4 = new Station("N4", 4);
        graph.addNode(n1);
        graph.addNode(n2);
        graph.addNode(n3);
        graph.addNode(n4);
        try {
            graph.addEdge(LabelledEdge.newInstance(n1, n2, "between1n2"));
            graph.addEdge(LabelledEdge.newInstance(n2, n3, "between2n3"));
            graph.addEdge(LabelledEdge.newInstance(n1, n3, "between1n3"));
            graph.addEdge(LabelledEdge.newInstance(n1, n4, "between1n4"));
            graph.addEdge(LabelledEdge.newInstance(n3, n4, "between3n4"));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }

        // Give basic SubwayRoute
        System.out.println("Construct route...");
        LabelledEdgePathFinder<Station, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
        MetroRoute sr = new MetroRoute(
                n1, n4, pf
        );
        System.out.println("Example route...");
        System.out.println(sr);

        // Make sure this is set up correctly
        System.out.println("Fist set of assertions...");
        assertEquals(sr.getStations().get(0).getLabel(), "between1n4");
        // Should only use one "Line"
        assertEquals(sr.getStations().size(), 1);

        // Change the origin
        System.out.println("Changing origin...");
        sr.newOrigin(n2);
        System.out.println(sr);
        assertEquals(sr.getStations().get(0).getLabel(), "between2n3");
        assertEquals(sr.getStations().get(1).getLabel(), "between3n4");
        assertEquals(sr.getStations().size(), 2);
    }

    @Test
    void newSRObjectTest() {
        // Set up graph
        System.out.println("Building graph...");
        LabelledMultiGraph<Station, String> graph = LabelledMultiGraph.newInstance();
        Station n1 = new Station("N1", 1);
        Station n2 = new Station("N2", 2);
        Station n3 = new Station("N3", 3);
        Station n4 = new Station("N4", 4);
        graph.addNode(n1);
        graph.addNode(n2);
        graph.addNode(n3);
        graph.addNode(n4);
        try {
            graph.addEdge(LabelledEdge.newInstance(n1, n2, "between1n2"));
            graph.addEdge(LabelledEdge.newInstance(n2, n3, "between2n3"));
            graph.addEdge(LabelledEdge.newInstance(n1, n3, "between1n3"));
            graph.addEdge(LabelledEdge.newInstance(n1, n4, "between1n4"));
            graph.addEdge(LabelledEdge.newInstance(n3, n4, "between3n4"));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }

        // Give basic SubwayRoute
        System.out.println("Construct route...");
        MetroRoute sr = new MetroRoute(
                n1, n4, new StickyShortestLabelledEdgePathFinder<>(graph)
        );
        System.out.println("Example route...");
        System.out.println(sr);

        // Make sure this is set up correctly
        System.out.println("Fist set of assertions...");
        assertEquals(sr.getStations().get(0).getLabel(), "between1n4");
        // Should only use one "Line"
        assertEquals(sr.getStations().size(), 1);

        // Change the origin
        System.out.println("Changing origin...");
        sr = new MetroRoute(n2, n4, new StickyShortestLabelledEdgePathFinder<>(graph));
        System.out.println(sr);
        assertEquals(sr.getStations().get(0).getLabel(), "between2n3");
        assertEquals(sr.getStations().get(1).getLabel(), "between3n4");
        assertEquals(sr.getStations().size(), 2);
    }
}
