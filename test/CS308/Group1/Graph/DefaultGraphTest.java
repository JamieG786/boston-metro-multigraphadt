package CS308.Group1.Graph;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DefaultGraphTest {

    private DefaultGraph<Integer, String> simpleGraph() throws NonExistentNodeException {
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();

        graph.addNode(1);
        graph.addNode(2);
        graph.addNode(3);
        graph.addNode(4);
        graph.addEdge(new DefaultEdge<>(1, 2, ""));
        graph.addEdge(new DefaultEdge<>(2, 3, ""));
        graph.addEdge(new DefaultEdge<>(3, 1, ""));
        graph.addEdge(new DefaultEdge<>(1, 4, ""));
        graph.addEdge(new DefaultEdge<>(4, 3, ""));

        return graph;
    }

    /*
        SECTION ONE - CREATION OF NODES
     */

    @Test
    void createSingleNode(){
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);

        List<Integer> nodesFromGraph = graph.getNodes();
        assertEquals(1, nodesFromGraph.size());
        assertTrue(nodesFromGraph.contains(1));
    }

    @Test
    void createMultipleNodes() {
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);

        List<Integer> nodesFromGraph = graph.getNodes();
        assertEquals(2, nodesFromGraph.size());
        assertTrue(nodesFromGraph.contains(1));
        assertTrue(nodesFromGraph.contains(2));
    }

    @Test
    void createIdenticalNodes(){
        // should not allow identical nodes
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(1);

        List<Integer> nodesFromGraph = graph.getNodes();
        assertEquals(1, nodesFromGraph.size());
        assertTrue(nodesFromGraph.contains(1));
    }

    /*
        SECTION TWO - CREATION OF EDGES
     */

    @Test
    void addSingleValidEdge(){
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        DefaultEdge<Integer, String> edge = new DefaultEdge<>(1, 2, "");
        assertDoesNotThrow(() -> graph.addEdge(edge));

        Collection<LabelledEdge<Integer, String>> edgesFromGraph = graph.getConnectingEdges(1, 2);
        assertEquals(1, edgesFromGraph.size());
        assertTrue(edgesFromGraph.contains(edge));
    }

    @Test
    void addLoopEdge(){
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        DefaultEdge<Integer, String> edge = new DefaultEdge<>(1, 1, "");
        assertDoesNotThrow(() -> graph.addEdge(edge));

        Collection<LabelledEdge<Integer, String>> edgesFromGraph = graph.getConnectingEdges(1, 1);
        assertEquals(1, edgesFromGraph.size());
        assertTrue(edgesFromGraph.contains(edge));
    }

    @Test
    void addMultipleEdgesDifferentNodes(){
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        graph.addNode(3);
        DefaultEdge<Integer, String> e1 = new DefaultEdge<>(1, 2, "");
        DefaultEdge<Integer, String> e2 = new DefaultEdge<>(3, 2, "");
        assertDoesNotThrow(() -> graph.addEdge(e1));
        assertDoesNotThrow(() -> graph.addEdge(e2));

        Collection<LabelledEdge<Integer, String>> edgesFromGraph = graph.getConnectingEdges(1, 2);
        edgesFromGraph.addAll(graph.getConnectingEdges(3, 2));

        assertEquals(2, edgesFromGraph.size());
        assertTrue(edgesFromGraph.contains(e1));
        assertTrue(edgesFromGraph.contains(e2));
    }

    @Test
    void addMultipleEdgesSameNodes(){
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        DefaultEdge<Integer, String> e1 = new DefaultEdge<>(1, 2, "a");
        DefaultEdge<Integer, String> e2 = new DefaultEdge<>(1, 2, "b");
        assertDoesNotThrow(() -> graph.addEdge(e1));
        assertDoesNotThrow(() -> graph.addEdge(e2));

        Collection<LabelledEdge<Integer, String>> edgesFromGraph = graph.getConnectingEdges(1, 2);

        assertEquals(2, edgesFromGraph.size());
        assertTrue(edgesFromGraph.contains(e1));
        assertTrue(edgesFromGraph.contains(e2));
    }

    @Test
    void addMultipleLoops(){
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        DefaultEdge<Integer, String> e1 = new DefaultEdge<>(1, 1, "a");
        DefaultEdge<Integer, String> e2 = new DefaultEdge<>(1, 1, "b");
        assertDoesNotThrow(() -> graph.addEdge(e1));
        assertDoesNotThrow(() -> graph.addEdge(e2));

        Collection<LabelledEdge<Integer, String>> edgesFromGraph = graph.getConnectingEdges(1, 1);

        assertEquals(2, edgesFromGraph.size());
        assertTrue(edgesFromGraph.contains(e1));
        assertTrue(edgesFromGraph.contains(e2));
    }

    @Test
    void addMultipleIdenticalEdges(){
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        DefaultEdge<Integer, String> e1 = new DefaultEdge<>(1, 2, "samelabel");
        DefaultEdge<Integer, String> e2 = new DefaultEdge<>(1, 2, "samelabel");
        assertEquals(e1, e2);
        assertDoesNotThrow(() -> graph.addEdge(e1));
        assertDoesNotThrow(() -> graph.addEdge(e2));

        Collection<LabelledEdge<Integer, String>> edgesFromGraph = graph.getConnectingEdges(1, 2);

        assertEquals(2, edgesFromGraph.size());
        assertTrue(edgesFromGraph.contains(e1));
    }

    /*
        SECTION THREE - GETTING GRAPH INFO
     */

    @Test
    void getSuccessorsSimpleGraph() throws NonExistentNodeException {
        DefaultGraph<Integer, String> graph = simpleGraph();
        for (Integer n :
                graph.getNodes()) {
            Set<Integer> successors = graph.getSuccessors(n);
            switch (n){
                case 1:
                    assertEquals(2, successors.size());
                    assertTrue(successors.contains(2));
                    assertTrue(successors.contains(4));
                    break;
                case 3:
                    assertEquals(1, successors.size());
                    assertTrue(successors.contains(1));
                    break;
                case 2: case 4:
                    assertEquals(1, successors.size());
                    assertTrue(successors.contains(3));
            }
        }
    }

    @Test
    void getSuccessorsDespiteMultipleEdges() throws NonExistentNodeException {
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        graph.addEdge(new DefaultEdge<>(1, 2, ""));
        graph.addEdge(new DefaultEdge<>(1, 2, ""));
        graph.addEdge(new DefaultEdge<>(1, 2, ""));

        Set<Integer> successors = graph.getSuccessors(1);

        assertEquals(1, successors.size());
        assertTrue(successors.contains(2));
    }

    @Test
    void containsEdge() throws NonExistentNodeException {
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        graph.addNode(3);
        DefaultEdge<Integer, String> e1 = new DefaultEdge<>(1, 2, "");
        DefaultEdge<Integer, String> e2 = new DefaultEdge<>(2, 1, "");
        DefaultEdge<Integer, String> e3 = new DefaultEdge<>(2, 3, "");
        graph.addEdge(e1);
        graph.addEdge(e2);

        assertTrue(graph.containsEdge(e1));
        assertTrue(graph.containsEdge(e2));
        assertFalse(graph.containsEdge(e3));
    }

    @Test
    void numberOfOutboundEdges() throws NonExistentNodeException {
        DefaultGraph<Integer, String> graph = simpleGraph();
        for (Integer n :
                graph.getNodes()) {
            Set<LabelledEdge<Integer, String>> outboundEdges = graph.getOutboundEdges(n);
            switch (n){
                case 1:
                    assertEquals(2, outboundEdges.size());
                    break;
                case 2: case 3: case 4:
                    assertEquals(1, outboundEdges.size());
            }
        }
    }

    @Test
    void onlyReturnsOutboundEdges() throws NonExistentNodeException {
        // not inbound ones
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        DefaultEdge<Integer, String> e1 = new DefaultEdge<>(1, 2, "");
        DefaultEdge<Integer, String> e2 = new DefaultEdge<>(2, 1, "");
        graph.addEdge(e1);
        graph.addEdge(e2);

        assertTrue(graph.getOutboundEdges(1).contains(e1));
        assertFalse(graph.getOutboundEdges(1).contains(e2));
        assertTrue(graph.getOutboundEdges(2).contains(e2));
        assertFalse(graph.getOutboundEdges(2).contains(e1));
    }

    @Test
    void numberOfInboundEdges() throws NonExistentNodeException {
        DefaultGraph<Integer, String> graph = simpleGraph();
        for (Integer n :
                graph.getNodes()) {
            Set<LabelledEdge<Integer, String>> outboundEdges = graph.getInboundEdges(n);
            switch (n){
                case 1: case 2: case 4:
                    assertEquals(1, outboundEdges.size());
                    break;
                case 3:
                    assertEquals(2, outboundEdges.size());
            }
        }
    }

    @Test
    void onlyReturnsInboundEdges() throws NonExistentNodeException {
        // not outbound ones
        DefaultGraph<Integer, String> graph = new DefaultGraph<>();
        graph.addNode(1);
        graph.addNode(2);
        DefaultEdge<Integer, String> e1 = new DefaultEdge<>(1, 2, "");
        DefaultEdge<Integer, String> e2 = new DefaultEdge<>(2, 1, "");
        graph.addEdge(e1);
        graph.addEdge(e2);

        assertTrue(graph.getInboundEdges(1).contains(e2));
        assertFalse(graph.getInboundEdges(1).contains(e1));
        assertTrue(graph.getInboundEdges(2).contains(e1));
        assertFalse(graph.getInboundEdges(2).contains(e2));
    }
}