package CS308.Group1.Graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefaultEdgeTest {

    DefaultEdge<Object, Object> edge;
    Object origin;
    Object destination;
    double weight;
    Object label;

    @BeforeEach
    void setup(){
        origin = new Object();
        destination = new Object();
        label = new Object();
        weight = 12.5;
        edge = new DefaultEdge<>(origin, destination, label, weight);
    }

    @Test
    void getLabel() {
        assertEquals(label, edge.getLabel());
    }

    @Test
    void from() {
        assertEquals(origin, edge.from());
    }

    @Test
    void to() {
        assertEquals(destination, edge.to());
    }

    @Test
    void getWeight() {
        assertEquals(weight, edge.getWeight());
    }

    @Test
    void setWeight() {
        edge.setWeight(-3.995);
        assertEquals(-3.995, edge.getWeight());
    }

    @Test
    void testEquals() {
        DefaultEdge<Object, Object> newedge = new DefaultEdge<>(origin, destination, label, weight);
        assertEquals(newedge, edge);

        newedge = new DefaultEdge<>(new Object(), destination, label, weight);
        assertNotEquals(newedge, edge);

        newedge = new DefaultEdge<>(origin, new Object(), label, weight);
        assertNotEquals(newedge, edge);

        newedge = new DefaultEdge<>(origin, destination, new Object(), weight);
        assertNotEquals(newedge, edge);

        newedge = new DefaultEdge<>(origin, destination, label, 12.6);
        assertNotEquals(newedge, edge);
    }
}