package CS308.Group1.Graph;

import CS308.Group1.Graph.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StickyShortestLabelledEdgePathFinderTest {

    @Test
    void unweightedIdenticalLabels(){
        LabelledMultiGraph<String, String> graph = LabelledMultiGraph.newInstance();
        graph.addNode("N1");
        graph.addNode("N2");
        graph.addNode("N3");
        graph.addNode("N4");
        try {
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N2", "N3",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N1", "N3",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N1", "N4",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N4", "N3",  "anedge"));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }

        StickyShortestLabelledEdgePathFinder<String, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
        List<LabelledEdge<String, String>> path = pf.findPath("N1", "N3");
        assertEquals(1, path.size());
        LabelledEdge<String, String> edge = path.get(0);
        assertEquals("N1", edge.from());
        assertEquals("N3", edge.to());
    }

    @Test
    void weightedIdenticalLabels(){
        LabelledMultiGraph<String, String> graph = LabelledMultiGraph.newInstance();
        graph.addNode("N1");
        graph.addNode("N2");
        graph.addNode("N3");
        graph.addNode("N4");
        try {
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N2", "N3",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N1", "N3",  "anedge", 3.0));
            graph.addEdge(LabelledEdge.newInstance("N1", "N4",  "anedge", 0.1));
            graph.addEdge(LabelledEdge.newInstance("N4", "N3",  "anedge", 0.1));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }

        StickyShortestLabelledEdgePathFinder<String, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
        List<LabelledEdge<String, String>> path = pf.findPath("N1", "N3");
        assertEquals(2, path.size());
        LabelledEdge<String, String> edge = path.get(0);
        assertEquals("N1", edge.from());
        assertEquals("N4", edge.to());
        edge = path.get(1);
        assertEquals("N4", edge.from());
        assertEquals("N3", edge.to());
    }

    @Test
    void weightedMultiLabelSingleLabelPossible(){
        LabelledMultiGraph<String, String> graph = LabelledMultiGraph.newInstance();
        graph.addNode("N1");
        graph.addNode("N2");
        graph.addNode("N3");
        graph.addNode("N4");
        try {
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N2", "N3",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N1", "N3",  "anedge"));
            graph.addEdge(LabelledEdge.newInstance("N1", "N4",  "A", 0.1));
            graph.addEdge(LabelledEdge.newInstance("N1", "N4",  "B", 0.1));
            graph.addEdge(LabelledEdge.newInstance("N1", "N4",  "C", 0.1));
            graph.addEdge(LabelledEdge.newInstance("N4", "N3",  "C", 0.2));
            graph.addEdge(LabelledEdge.newInstance("N4", "N3",  "B", 0.2));
            graph.addEdge(LabelledEdge.newInstance("N4", "N3",  "A", 0.2));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }

        StickyShortestLabelledEdgePathFinder<String, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
        List<LabelledEdge<String, String>> path = pf.findPath("N1", "N3");
        assertEquals(2, path.size());
        LabelledEdge<String, String> edge1 = path.get(0);
        assertEquals("N1", edge1.from());
        assertEquals("N4", edge1.to());
        LabelledEdge<String, String> edge2 = path.get(1);
        assertEquals("N4", edge2.from());
        assertEquals("N3", edge2.to());
        assertEquals(edge1.getLabel(), edge2.getLabel());

        path = pf.findPath("N1", "N4");
        assertEquals(1, path.size());
        edge1 = path.get(0);
        assertEquals("N1", edge1.from());
        assertEquals("N4", edge1.to());
    }

    @Test
    void pathDoesNotExist(){
        LabelledMultiGraph<String, String> graph = LabelledMultiGraph.newInstance();
        graph.addNode("N1");
        graph.addNode("N2");
        graph.addNode("N3");
        try {
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "anedge"));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }

        StickyShortestLabelledEdgePathFinder<String, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
        List<LabelledEdge<String, String>> path = pf.findPath("N1", "N3");
        assertEquals(0, path.size());
    }

    @Test
    void choosesRightPathFromBeginning(){
        LabelledMultiGraph<String, String> graph = LabelledMultiGraph.newInstance();
        graph.addNode("N1");
        graph.addNode("N2");
        graph.addNode("N3");
        try {
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "A"));
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "B"));
            graph.addEdge(LabelledEdge.newInstance("N2", "N3",  "B"));

            StickyShortestLabelledEdgePathFinder<String, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
            List<LabelledEdge<String, String>> path = pf.findPath("N1", "N3");
            assertEquals(2, path.size());
            assertEquals("B", path.get(0).getLabel());
            assertEquals("B", path.get(1).getLabel());
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }
    }

    @Test
    void doesntSwitchUnnecessarilyInMiddle(){
        LabelledMultiGraph<String, String> graph = LabelledMultiGraph.newInstance();
        graph.addNode("N1");
        graph.addNode("N2");
        graph.addNode("N3");
        graph.addNode("N4");
        graph.addNode("N5");
        graph.addNode("N6");
        try {
            graph.addEdge(LabelledEdge.newInstance("N5", "N3",  "B"));
            graph.addEdge(LabelledEdge.newInstance("N2", "N5",  "B"));
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "A"));
            graph.addEdge(LabelledEdge.newInstance("N2", "N6",  "A"));
            graph.addEdge(LabelledEdge.newInstance("N6", "N3",  "A"));
            graph.addEdge(LabelledEdge.newInstance("N3", "N4",  "A"));

            StickyShortestLabelledEdgePathFinder<String, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
            List<LabelledEdge<String, String>> path = pf.findPath("N1", "N4");
            assertEquals(4, path.size());
            assertTrue(path.stream().map(e -> e.getLabel().equals("B")).allMatch(aBoolean -> true));
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }
    }

    @Test
    void willTakeAShortcutInMiddle(){
        LabelledMultiGraph<String, String> graph = LabelledMultiGraph.newInstance();
        graph.addNode("N1");
        graph.addNode("N2");
        graph.addNode("N3");
        graph.addNode("N4");
        graph.addNode("N5");
        graph.addNode("N6");
        try {
            graph.addEdge(LabelledEdge.newInstance("N5", "N3",  "B", 0.99)); // shortcut
            graph.addEdge(LabelledEdge.newInstance("N2", "N5",  "B", 0.99));
            graph.addEdge(LabelledEdge.newInstance("N1", "N2",  "A"));
            graph.addEdge(LabelledEdge.newInstance("N2", "N6",  "A"));
            graph.addEdge(LabelledEdge.newInstance("N6", "N3",  "A"));
            graph.addEdge(LabelledEdge.newInstance("N3", "N4",  "A"));

            StickyShortestLabelledEdgePathFinder<String, String> pf = new StickyShortestLabelledEdgePathFinder<>(graph);
            List<LabelledEdge<String, String>> path = pf.findPath("N1", "N4");
            assertEquals(4, path.size());
            assertEquals("A", path.get(0).getLabel());
            assertEquals("B", path.get(1).getLabel());
            assertEquals("B", path.get(2).getLabel());
            assertEquals("A", path.get(3).getLabel());
        } catch (NonExistentNodeException e) {
            e.printStackTrace();
        }
    }
}