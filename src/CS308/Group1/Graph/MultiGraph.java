package CS308.Group1.Graph;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * A collection of nodes of type <tt>N</tt> connected by directed, weighted edges.
 * <p>
 * Anything may be stored at a node, this provides maximum flexibility.
 * Nodes must be distinct, a graph cannot contain the same node twice.
 * Nodes are considered the same if for every pair of nodes <code>n1</code> and
 * <code>n2</code> then <code>(n1==null ? n2==null : n1.equals(n2))</code> is
 * <code>true</code>. It is not enforced that node data is non-null, so in theory
 * a graph could contain a single node with null data, however implementations should
 * feel free to add this restriction.
 * <p>
 * It is recommended that implementations do not allow the removal of edges and nodes from
 * the graph. This is to reduce the likelihood where some intensive computation is performed
 * on the graph (for example a path-finding operation) and before the result is used a node or
 * edge is removed from the graph.
 * <p>
 * Multiple edges with the same weight connecting the same two nodes are permissible.
 * <p>
 * Computations should be kept to a minimum in implementations of this datatype and
 * should instead be delegated to other types that perform computations on a
 * <tt>MultiGraph</tt> that is supplied to them. For an example see
 * {@link LabelledEdgePathFinder}
 * and {@link StickyShortestLabelledEdgePathFinder}
 *
 * @param <N> the type stored in each node of the graph
 */
public interface MultiGraph<N> {

    /**
     * Return a list of all the nodes in the graph
     *
     * @return a list of all nodes in the graph
     */
    List<N> getNodes();

    /**
     * Creates a new node and adds it to the graph.
     * If the node already exists in the graph it will not be added again.
     *
     * @param data the data to be stored at the node
     */
    void addNode(N data);

    /**
     * Adds an edge to the graph.
     *
     * <p>Multiple edges are allowed, even if they are identically weighted.
     * Edges must connect nodes that already exist in the graph.</p>
     *
     * @param edge to be added to the graph
     * @throws NonExistentNodeException if the edge attempting to be added connects
     *                                  a node that does not exist in the graph
     */
    void addEdge(DirectedWeightedEdge<N> edge) throws NonExistentNodeException;

    /**
     * Returns the set of all nodes reachable from the <code>target</code> by traversing a single edge.
     * <p>
     * If the graph contains a loop, it is possible that the <code>target</code> itself is in the returned set.
     * <p>
     *
     * @param target the node for which the successors are required
     * @return the set of successors to the <code>target</code>
     */
    Set<N> getSuccessors(N target);

    /**
     * Returns the collection of edges that directly connect from the <tt>origin</tt> to
     * the <tt>dest</tt>.
     * <p>
     * The <tt>origin</tt> and <tt>dest</tt> are permitted to be the same node so as
     * to get the set of loops.
     * <p>
     * If there are no connecting edges the empty set is returned.
     *
     * @param origin the source edge
     * @param dest   the destination edge
     * @return the collection of edges that connect from the source to the destination
     */
    Collection<? extends DirectedWeightedEdge<N>> getConnectingEdges(N origin, N dest);

    /**
     * Returns true if a given edge exists in the graph
     *
     * @param target the edge with which to query the graph
     * @return true if the edge exists in the graph
     */
    boolean containsEdge(DirectedWeightedEdge<N> target);

    /**
     * Returns a set of all the outbound edges of a given node
     *
     * @param node the node for which we want all the outbound edges
     * @return the set of all outbound edges of the given node
     */
    Set<? extends DirectedWeightedEdge<N>> getOutboundEdges(N node);

    /**
     * Returns a set of all the inbound edges to a given node
     *
     * @param node the node for which we want all the inbound edges
     * @return the set of all inbound edges of the given node
     */
    Set<? extends DirectedWeightedEdge<N>> getInboundEdges(N node);

}
