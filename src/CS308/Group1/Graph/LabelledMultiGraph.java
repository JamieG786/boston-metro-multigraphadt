package CS308.Group1.Graph;

import java.util.Collection;
import java.util.Set;

/**
 * Represents a Multigraph where the edges must be {@link LabelledEdge}s.
 * <p>
 * Methods that, in the Multigraph interface, were specified to return
 * some data structure containing a subclass of DirectedWeightedEdge
 * here are specified as returning LabelledEdge, this is what defines
 * the graph as being a labelled graph.
 * <p>
 * A static factory method allows the creation of a default implementation.
 *
 * @param <N> The type of node
 * @param <E> The type of edge label
 */
public interface LabelledMultiGraph<N, E> extends MultiGraph<N> {

    /**
     * Returns a default implementation of this interface.
     * <p>
     * No guarantees about the returned object are made except that it
     * implements this interface and ,of course, inherited interfaces.
     * <p>
     * This static factory method is used to avoid dependencies on a
     * concrete class.
     *
     * @param <N> The type of node
     * @param <E> The type of edge label
     * @return A new object that implements this interface.
     */
    static <N, E> LabelledMultiGraph<N, E> newInstance() {
        return new DefaultGraph<>();
    }

    @Override
    Collection<LabelledEdge<N, E>> getConnectingEdges(N origin, N dest);

    @Override
    Set<LabelledEdge<N, E>> getOutboundEdges(N node);

    @Override
    Set<LabelledEdge<N, E>> getInboundEdges(N node);
}
