package CS308.Group1.Graph;

/**
 * A directed, weighted edge that has associated label data.
 * <p>
 * Any object can be used to label an edge.
 *
 * @param <N> the type of node the edge connects
 * @param <E> the type of label associated with the edge
 */
public interface LabelledEdge<N, E> extends DirectedWeightedEdge<N> {


    /**
     * Returns a default implementation of this interface.
     * <p>
     * No guarantees about the returned object are made except that it
     * implements this interface and ,of course, inherited interfaces.
     *
     * @param from   the source node where the edge originates
     * @param to     the target node where the edge terminates
     * @param label  the label data associated with the edge
     * @param weight the weight/cost of the edge
     * @param <N>    The type of node
     * @param <E>    The type of edge label
     * @return A new object that implements this interface
     */
    static <N, E> LabelledEdge<N, E> newInstance(N from, N to, E label, double weight) {
        return new DefaultEdge<>(from, to, label, weight);
    }

    /**
     * Returns a default implementation of this interface with a default weight
     * of 1.0.
     * <p>
     * No guarantees about the returned object are made except that it
     * implements this interface and ,of course, inherited interfaces.
     *
     * @param from   the source node where the edge originates
     * @param to     the target node where the edge terminates
     * @param label  the label data associated with the edge
     * @param <N>    The type of node
     * @param <E>    The type of edge label
     * @return A new object that implements this interface
     */
    static <N, E> LabelledEdge<N, E> newInstance(N from, N to, E label) {
        return new DefaultEdge<>(from, to, label, 1.0);
    }

    /**
     * Returns the value of the labelled associated with the edge.
     * <p>
     * Label values are non-null.
     *
     * @return the value of the label associated with this edge.
     */
    E getLabel();

}
