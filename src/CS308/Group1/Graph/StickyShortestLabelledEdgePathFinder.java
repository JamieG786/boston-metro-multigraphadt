package CS308.Group1.Graph;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An implementation of {@link LabelledEdgePathFinder} that finds the shortest possible
 * path between two nodes in a weighted, labelled graph while also minimising the amount
 * of different labels encountered.
 *
 * The algorithm used is a variant of Uniform Cost Search.
 * It diverges from Uniform Cost Search in two regards:
 * <p>
 * 1) When choosing the "next node" from the agenda, if there are multiple nodes with
 *    equally short paths-so-far, it will endeavour to firstly choose a node that is on
 *    the same line as the shortest path to its parent. I.e. it will "stick" to the same
 *    edge label where it will not sacrifice the shortest path to do so.
 * <p>
 * 2) When there are multiple, equivalently short, edges connecting a pair of nodes,
 *    n1 to n2 the edge is selected according to a list of four priorities attempted in order
 *    by the following method {@link #getStickyShortestEdge}. See comment on
 *    this method for detail on how it selects the edge
 *
 * This technique allows us to always find the minimum weighted path while also trying
 * to minimise the amount of labels encountered on a path.
 *
 * @param <N> the type of data stored at each node in the path
 * @param <E> the type of the labels of each edge in the path
 */
public class StickyShortestLabelledEdgePathFinder<N, E extends Comparable<E>> implements LabelledEdgePathFinder<N, E> {

    private LabelledMultiGraph<N, E> graph;
    private Map<N, Double> currentShortestDistances;
    private Map<N, LabelledEdge<N, E>> previousNode;
    private List<E> destinationLabels;

    /**
     * Create a new pathfinder on the required graph
     *
     * @param graph where we wish to find paths
     */
    public StickyShortestLabelledEdgePathFinder(LabelledMultiGraph<N, E> graph) {
        this.graph = graph;
        currentShortestDistances = new HashMap<>();
        previousNode = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     *
     * The path returned will always be minimally weighted and will try to encounter
     * as small a number of distinct edge labels as it can.
     *
     * @param origin      the node that should be at the beginning of the path
     * @param destination the node that should be at the end of the path
     * @return a list of edges that when traversed, in order, go from the origin to the destination
     */
    @Override
    public List<LabelledEdge<N, E>> findPath(N origin, N destination) {
        // If the nodes are not in the graph there will never be a path
        // so bail early
        List<N> nodes = graph.getNodes();
        if(!nodes.contains(origin) || !nodes.contains(destination)){
            return new ArrayList<>();
        }

        Set<N> settledNodes = new HashSet<>();
        Set<N> unsettledNodes = new HashSet<>();
        destinationLabels = inboundLabels(destination);

        currentShortestDistances.clear();

        unsettledNodes.add(origin);
        currentShortestDistances.put(origin, 0.0);
        // for all unsettled nods
        while(!unsettledNodes.isEmpty()){
            // get the closest one to the 'frontier'
            N node = getNextNode(unsettledNodes);
            // leave the loop early if its time to 'settle' our destination
            if(node.equals(destination)){
                break;
            }
            // otherwise settle this node
            settledNodes.add(node);

            // and update the values of all successors to our new settled node
            Set<N> successors = graph.getSuccessors(node);
            for (N successor :
                    successors) {
                // if the edge isn't already settled, add it to unsettled set and if this way of
                // reaching it is smaller, update the previous nodes and shortest distance
                if(!settledNodes.contains(successor)){
                    unsettledNodes.add(successor);
                    LabelledEdge<N, E> shortestEdge = getStickyShortestEdge(node, successor);
                    double newDistance = currentShortestDistances.get(node) + shortestEdge.getWeight();
                    if(newDistance < currentShortestDistances.getOrDefault(successor, Double.MAX_VALUE)){
                        previousNode.put(successor, shortestEdge);
                        currentShortestDistances.put(successor, newDistance);
                    }
                }
            }
        }

        return buildPath(origin, destination);
    }

    /**
     * Returns a list of labels of all inbound adjacent edges to a particular node
     *
     * @param destination the node for which the list of labels is required
     * @return the list of labels of all inbound adjacent edges to a node
     */
    private List<E> inboundLabels(N destination) {
        return graph.getInboundEdges(destination)
                .stream()
                .map(LabelledEdge::getLabel)
                .collect(Collectors.toList());
    }

    /**
     * This is where we deviate slightly from Dijkstra's algorithm, this part is what makes
     * it "sticky"
     *
     * When there are multiple, equivalently short, edges connecting a pair of nodes,
     *    n1 to n2 the edge is selected according to the following priority - in order -
     *    i)   If one of the edges labels in the same as that of the last edge in the shortest
     *         path to n1 AND that edge label is also one which appears on an inbound edge of
     *         the final destination node, pick it. (i.e. stick to the same label as previously
     *         where it will lead to a destination).
     *         Otherwise...
     *    ii)  If there is a choice of switching from any edge that is NOT labelled the same
     *         as an inbound edge to the destination, to one that is switch to it.
     *         (I.e. switch to a new label that will lead to a destination where you can).
     *         Otherwise...
     *    iii) If one of the edges has the same label as the last edge in the shortest path
     *         to n1, choose the same label again. I.e. where you can't switch to the ultimate
     *         edge label at least stick to the same edge label your on where it won't cost more.
     *         Otherwise...
     *    iv)  Pick any edge.
     *
     * @param node the origin node
     * @param adjNode the destination node
     * @return the chosen shortest edge between them according to the criteria above
     */
    private LabelledEdge<N, E> getStickyShortestEdge(N node, N adjNode) {
        Collection<LabelledEdge<N, E>> connectingEdges = graph.getConnectingEdges(node, adjNode);
        List<LabelledEdge<N, E>> smallestEdges = getSmallestEdges(connectingEdges);
        List<E> smallestEdgeLabels = smallestEdges.stream().map(LabelledEdge::getLabel).collect(Collectors.toList());
        LabelledEdge<N, E> previousEdge = previousNode.get(node);
        E previousLabel = previousEdge != null ? previousEdge.getLabel() : null;

        // where we have a choice for which smallest edge to choose choose according to
        // the following priorities (to minimise switching)

        // 1) if the previous label is available and will lead to dest choose it
        if(previousLabel != null
                && destinationLabels.contains(previousLabel)
                && smallestEdgeLabels.contains(previousLabel)){
            return smallestEdges.stream()
                    .reduce(smallestEdges.get(0),
                            (e1, e2) -> e1.getLabel().equals(previousLabel) ? e1 : e2);
        } // otherwise ...

        // 2) if we can switch to an edge that will lead to dest switch
        List<E> intersection = new ArrayList<>(destinationLabels);
        intersection.retainAll(smallestEdgeLabels);
        intersection = intersection.stream().distinct().collect(Collectors.toList());
        if(intersection.size() > 0){
            E chosenLabel = chooseLabel(intersection);
            return smallestEdges.stream()
                    .reduce(smallestEdges.get(0),
                            (e1, e2) -> e1.getLabel().equals(chosenLabel) ? e1 : e2);
        } // otherwise ...

        // 3) if we can stay on the same label do so
        if(smallestEdgeLabels.contains(previousLabel)){
            return smallestEdges.stream()
                    .reduce(smallestEdges.get(0),
                            (e1, e2) -> e1.getLabel().equals(previousLabel) ? e1 : e2);
        } // otherwise ...

        // 4) Choose edge with shortest label (least specialized)
        return smallestEdges.stream()
                .reduce(smallestEdges.get(0),
                        (e1, e2) -> e1.getLabel().compareTo(e2.getLabel()) <= 0 ? e1 : e2);
    }

    /**
     * Chooses a preferred label from a list.
     *
     * @param intersection the list of labels from which it will choose
     * @return the chosen label
     */
    private E chooseLabel(List<E> intersection) {
        if(intersection.size()==2 || intersection.size()==4){
            return intersection.stream().max(Comparator.naturalOrder()).get();
        }
        return intersection.stream().min(Comparator.naturalOrder()).orElse(intersection.get(0));
    }

    /**
     * Returns an exhaustive list of minimally weighted edges in a set.
     *
     * All edges in the returned list will have a weight equal to each other and
     * no edge in the provided set will have a smaller weight.
     *
     * @param edges the set of edges were the minimally weighted result is taken
     * @return an exhaustive list of minimally weighted edges
     */
    private List<LabelledEdge<N,E>> getSmallestEdges(Collection<LabelledEdge<N,E>> edges) {
        double minWeight = edges.stream()
                .map(LabelledEdge::getWeight)
                .min(Double::compareTo)
                .orElse(Double.MAX_VALUE);
        return edges.stream()
                .filter(e -> e.getWeight()==minWeight)
                .collect(Collectors.toList());
    }

    /**
     * Gets the next closest node according to the algorithm
     *
     * Where we have a choice of next nodes that are the same distance,
     * choose the one that uses the same label as was previously used
     * if possible.
     *
     * @param unsettledNodes nodes yet to be settled
     * @return the closest node to the frontier (next target)
     */
    private N getNextNode(Set<N> unsettledNodes) {
        // if there is only a single node in the agenda, remove and return it
        if(unsettledNodes.size()==1){
            N node = new ArrayList<>(unsettledNodes).get(0);
            unsettledNodes.remove(node);
            return node;
        }

        // otherwise find the minimally distanced node to take next
        N closestNode = null;
        double currentShortestDistance = Integer.MAX_VALUE;

        // for every unsettled node
        for (N node :
                unsettledNodes) {
            // if its closer than our current shortest, it becomes the shortest
            double distance = currentShortestDistances.get(node);
            if(distance < currentShortestDistance){
                closestNode = node;
                currentShortestDistance = distance;
                continue;
            }

            // otherwise find out the previous label, and the previous label of its
            // predecessor on the shortest path to it
            LabelledEdge<N, E> currentShortestEdge = previousNode.get(node);
            E edgeLabel = currentShortestEdge.getLabel();
            LabelledEdge<N, E> predecessorEdge = previousNode.get(currentShortestEdge.from());
            E previousEdgeLabel = predecessorEdge == null ? null : predecessorEdge.getLabel();

            // if it is the same distance as the current shortest but doesn't involve
            // changing labels it becomes the new shortest
            if(distance == currentShortestDistance && edgeLabel.equals(previousEdgeLabel)){
                closestNode = node;
                currentShortestDistance = distance;
            }
        }
        unsettledNodes.remove(closestNode);
        return closestNode;
    }

    /**
     * Attempts to build the shortest path between two nodes from the information
     * generated in the path finding process.
     *
     * Returns a list of edges that when traversed, in order, connects one node
     * to another.
     *
     * If no path is found, returns an empty list
     *
     * @param from the origin node
     * @param to the destination node
     * @return a list of edges that when traversed, in order, connects origin to destination
     */
    private List<LabelledEdge<N,E>> buildPath(N from, N to) {
        List<LabelledEdge<N, E>> shortestPath = new ArrayList<>();
        N currentTarget = to;
        while(currentTarget != from){
            LabelledEdge<N, E> edge = previousNode.get(currentTarget);
            // there was no path between the nodes return empty list
            if(edge==null){
                return new ArrayList<>();
            }
            shortestPath.add(0, edge);
            currentTarget = edge.from();
        }
        return shortestPath;
    }
}
