package CS308.Group1.Graph;

/**
 * Thrown if it is found that a {@link DirectedWeightedEdge} in a
 * {@link MultiGraph} refers to a node that does not exist in that graph.
 */
public class NonExistentNodeException extends Exception {

    public NonExistentNodeException(String message) {
        super(message);
    }
}
