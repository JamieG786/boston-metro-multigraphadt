package CS308.Group1.Graph;

import java.util.Objects;

/**
 * A default implementation of {@link LabelledEdge}
 * <p>
 * Such an edge must always be given a label, however a weight may
 * optionally be omitted. When a weight is not specified a default
 * weight of 1.0 is assigned to the edge.
 *
 * @param <N> the type of nodes the edge connects
 * @param <E> the type of label associated with the edge
 */
class DefaultEdge<N, E> implements LabelledEdge<N, E> {

    private N from;
    private N to;
    private E label;
    private double weight;

    /**
     * Constructs a new DefaultEdge with the specified label and weight
     * that connects the specified nodes
     *
     * @param from   the source node where the edge originates
     * @param to     the target node where the edge terminates
     * @param label  the label data associated with the edge
     * @param weight the weight/cost of the edge
     */
    public DefaultEdge(N from, N to, E label, double weight) {
        this.from = from;
        this.to = to;
        this.label = label;
        this.weight = weight;
    }

    /**
     * Constructs a new DefaultEdge with the specified label that
     * connects the specified nodes.
     * <p>
     * A default weight of 1.0 is assigned to the edge.
     *
     * @param from  the source node where the edge originates
     * @param to    the target node where the edge terminates
     * @param label the label data associated with the edge
     */
    public DefaultEdge(N from, N to, E label) {
        this.from = from;
        this.to = to;
        this.label = label;
        this.weight = 1.0;
    }

    @Override
    public E getLabel() {
        return label;
    }

    @Override
    public N from() {
        return from;
    }

    @Override
    public N to() {
        return to;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Edges are considered equal the nodes they connect are considered equal
     * their weight is considered equal and their label is considered equal
     *
     * @param o the object with which this object is being tested for equality
     * @return true if they are considered equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultEdge<?, ?> that = (DefaultEdge<?, ?>) o;
        return Double.compare(that.weight, weight) == 0 &&
                from.equals(that.from) &&
                to.equals(that.to) &&
                label.equals(that.label);
    }

    /**
     * If objects are equal they must have the same hashcode
     *
     * @return the hashcode of this edge
     */
    @Override
    public int hashCode() {
        return Objects.hash(from, to, label, weight);
    }
}
