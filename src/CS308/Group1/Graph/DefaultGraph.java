package CS308.Group1.Graph;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class implements the {@link MultiGraph} interface but with the restriction
 * that it will only allow {@link LabelledEdge}s to be added to the graph. An attempt
 * to add an edge that is not of this type will result in an {@link UnsupportedOperationException}.
 * <p>
 * The implementation of any abstract method that was to return
 * <code>{@link Set}&lt? extends {@link DirectedWeightedEdge}&gt</code> in this implementation
 * returns {@link Set}<{@link LabelledEdge}> explicitly.
 *
 * @param <N> the type of nodes in the graph
 * @param <E> the type of the label associated with each edge
 */
class DefaultGraph<N, E> implements LabelledMultiGraph<N, E> {

    private Set<N> nodes;
    private Collection<LabelledEdge<N, E>> edges;

    /**
     * Constructs a new empty LabelledGraph with no nodes or edges.
     */
    public DefaultGraph() {
        this.nodes = new HashSet<>();
        this.edges = new ArrayList<>(); // as edges can contain duplicates
    }

    @Override
    public List<N> getNodes() {
       return new ArrayList<>(nodes);
    }

    @Override
    public void addNode(N data) {
        // nodes is a SET so we don't need to check for existence first
        nodes.add(data);
    }

    /**
     * {@inheritDoc}
     *
     * Only allows the addition of {@link LabelledEdge}s.
     *
     * @throws UnsupportedOperationException if the edge being added is not a {@link LabelledEdge}
     */
    @Override
    public void addEdge(DirectedWeightedEdge<N> edge) throws NonExistentNodeException {
        if (edge instanceof LabelledEdge) {
            if (nodes.contains(edge.from()) && nodes.contains(edge.to())) {
                edges.add((LabelledEdge<N, E>) edge);
            } else {
                throw new NonExistentNodeException("Edge connects a node that does not exist in the graph.");
            }
        } else {
            throw new UnsupportedOperationException("Cannot add an unlabelled edge to a labelled graph.");
        }
    }

    @Override
    public Set<N> getSuccessors(N target) {
        return edges.stream()
                .filter(e -> e.from().equals(target))
                .map(DirectedWeightedEdge::to)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean containsEdge(DirectedWeightedEdge<N> target) {
        return edges.stream().anyMatch(e -> e.equals(target));
    }

    @Override
    public Collection<LabelledEdge<N, E>> getConnectingEdges(N origin, N dest) {
        return edges.stream()
                .filter(e -> e.from().equals(origin) && e.to().equals(dest))
                .collect(Collectors.toList());
    }

    @Override
    public Set<LabelledEdge<N, E>> getOutboundEdges(N node) {
        return edges.stream()
                .filter(x -> x.from().equals(node))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<LabelledEdge<N, E>> getInboundEdges(N node) {
        return edges.stream()
                .filter(x -> x.to().equals(node))
                .collect(Collectors.toSet());
    }

}
