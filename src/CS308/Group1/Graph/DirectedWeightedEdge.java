package CS308.Group1.Graph;

/**
 * A weighted edge connect from some <tt>N</tt> to another <tt>N</tt>
 * <p>
 * The weight is mutable.
 * <p>
 * An edge can connect a node to itself.
 *
 * @param <N> the type of data stored at the nodes it connects
 */
public interface DirectedWeightedEdge<N> {

    /**
     * Returns the source of the edge (the node where it originates from)
     *
     * @return the node where the edge originates from
     */
    N from();

    /**
     * Returns the destination of the edge (where it is connecting to)
     *
     * @return the node where the head is heading to
     */
    N to();

    /**
     * Returns the weight associated with the edge
     *
     * @return the weight associated with the edge
     */
    double getWeight();

    /**
     * Updates the weight of the edge with the newly specified weight
     *
     * @param weight the new weight of the edge
     */
    void setWeight(double weight);

}
