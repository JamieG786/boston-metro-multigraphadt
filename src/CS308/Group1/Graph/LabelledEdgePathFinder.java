package CS308.Group1.Graph;

import CS308.Group1.Graph.LabelledEdge;

import java.util.List;

/**
 * Used to find paths in a graph with labelled edges.
 * <p>
 * Paths are returned as a list of edges.
 * This gives all necessary path information as edges can be queried to find
 * their connecting nodes. This is favoured over returning a list of nodes as
 * it allows a PathFinder to return paths relating to MultiGraphs where a list of
 * nodes alone might not contain enough information to specify a unique path in the graph.
 * <p>
 * Implementations could prioritize different aspects, for instance
 * least number of edges traversed or weight/cost minimization.
 * <p>
 * It is expected that an implementation would have access to some graph data.
 *
 * @param <N>
 */
public interface LabelledEdgePathFinder<N, E> {

    /**
     * Returns a list of edges, in a specified order, such that if we begin at
     * the <tt>origin</tt> and traverse each edge, in order, we arrive at the
     * <tt>destination</tt>.
     * <p>
     * A path is taken to be at least one edge, so if the origin and the destination
     * are the same, a path only exists if there is a loop on the node.
     * <p>
     * If no path is found, an empty list is returned.
     * <p>
     * A path returned must always be correct, but is not restricted to be optimal
     * according to any particular metric by the interface. It is up to the implementing
     * class to decide what properties should be enforced on the returned path.
     *
     * @param origin      the node that should be at the beginning of the path
     * @param destination the node that should be at the end of the path
     * @return a list of edges that when traversed, in order, go from the origin to the destination
     */
    List<LabelledEdge<N, E>> findPath(N origin, N destination);

}
