package CS308.Group1.BostonMetro;

/**
 * Represents a station that has a name and and id.
 */
public class Station implements Comparable<Station> {
    private String name;
    private int id;

    /**
     * Creates a station from a given non-null name and id
     *
     * @param name of the station
     * @param id   of the station
     */
    public Station(String name, int id) {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.id = id;
    }

    /**
     * Returns the name of the station
     *
     * @return the name of the station
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the id of the station
     *
     * @return the station id
     */
    public int getId() {
        return id;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Stations are compared by name
     * <p>
     * id's are arbitrary and it is most useful to sort stations
     * into alphabetical order.
     */
    @Override
    public int compareTo(Station o) {
        // So that we can sort stations into ascending
        // order by name
        return name.compareTo(o.getName());
    }
}
