package CS308.Group1.BostonMetro;

import java.util.Scanner;

/**
 * The interface to the application.
 * <p>
 * Prints greetings, farewells and controls the interaction with the user.
 */
public class CommandLineInterface {

    private static final String GREETING = "-----------------------------------------------" +
            "\nHello! Welcome to the Boston Metro Application!" +
            "\nRed line costs 0.2 per stop\nGreen/Blue lines cost 1 per stop\nOrange line costs 2 per stop" +
            "\n-----------------------------------------------";
    private static final String GOODBYE = "Thank you for using the Application! Goodbye!";

    /**
     * Main interface control loop.
     * <p>
     * Greet the user, service them as many times as they want then say goodbye.
     *
     * @param router   The MetroRoute that prints metro directions
     * @param selector for getting a user to select stations
     */
    public void run(MetroRoute router, StationSelector selector, Scanner scanner) {
        System.out.println(GREETING);
        while (respondToUserQuery(selector, router, scanner)) ;
        System.out.println(GOODBYE);
    }

    /**
     * Prints subway directions for a user after getting a origin and destination
     * station from them.
     * <p>
     * Returns false if the user expressed a wish to exit the application otherwise
     * returns true.
     *
     * @param selector for getting the user to choose subway stations
     * @param router   for generating subway directions
     * @return false if the user expressed a wish to exit the application
     */
    private boolean respondToUserQuery(StationSelector selector, MetroRoute router, Scanner scanner) {
        // Get the selected origin station from the user
        Station origin = selector.getUserSelection("origin");
        if (origin == null) {
            return false;
        }
        // Get the selected destination station from the user
        Station destination = selector.getUserSelection("destination");
        if (destination == null) {
            return false;
        }

        //calculate and print the route
        router.newOrigin(origin);
        router.newDestination(destination);
        System.out.println(router.condensedDirections());

        System.out.print("\nWould you like to see a detailed view of the route? (y/n): ");
        String response = scanner.next().toLowerCase().trim();
        if(response.equals("y") || response.equals("yes")){
            System.out.println("\n" + router);
        }

        return true;
    }


}
