package CS308.Group1.BostonMetro;


import CS308.Group1.Graph.LabelledEdge;
import CS308.Group1.Graph.LabelledMultiGraph;
import CS308.Group1.Graph.NonExistentNodeException;

import java.io.*;
import java.util.*;

/**
 * This class reads a text description of a metro subway system
 * and generates a graph representation of the metro.
 * <p>
 * A typical line in the file looks like this :
 * <code> 20 NorthStation   Green 19 22  Orange 15 22  </code>
 * where :
 * 20 is the StationID <p>
 * NorthStation is the StationName<p>
 * Green 19 22 is a LineID in which :<p>
 * Green is the LineName<p>
 * 19 is the StationID of the outbound station<p>
 * 22 is the StationID of the inbound station<p>
 * Orange 15 22 is a LineID in which :<p>
 * Orange is the LineName<p>
 * 15 is the StationID of the outbound station<p>
 * 22 is the StationID of the inbound station<p>
 * <p>
 * Therefore, NorthStation has two outgoing lines.
 * note : 0 denotes the end of a line : i.e. in this case,
 * OakGrove would be at the end of the line, as there is no other outbound
 * station.
 */

public class MetroMapParser {

    private BufferedReader stationFileInput;
    private BufferedReader lineFileInput;

    private List<Station> stations;
    private Map<Integer, Station> stationMap;

    /**
     * creates a new parser that will read from the file
     * filename unless the file does not exist.
     *
     * @param filename The name of the file
     */

    public MetroMapParser(String filename) {
        // We access the file this way so that we can package up the file inside the jar later
        Reader reader = new InputStreamReader(getClass().getResourceAsStream("/"+filename));
        stationFileInput = new BufferedReader(reader);
        reader = new InputStreamReader(getClass().getResourceAsStream("/"+filename));
        lineFileInput = new BufferedReader(reader);
        stations = new ArrayList<>();
    }

    /**
     * parses the file, and populates a graph from it, unless there
     * is a problem reading the file, or there is a problem with the format of the
     * file.
     * <p>
     *
     * @param graph To be populated with
     * @throws java.io.IOException if there is a problem reading the file
     */
    public void generateGraphFromFile(LabelledMultiGraph<Station, String> graph)
            throws IOException {
        String line = stationFileInput.readLine();
        StringTokenizer st;
        stationMap = new HashMap<>();

        // firstly parse all the station nodes
        while (line != null) {
            st = new StringTokenizer(line);
            try {
                Station station = parseStation(st);
                addStation(graph, station);
            } catch (BadFileException e) {
                System.out.println("Bad line in file. Line ignored.");
                e.printStackTrace();
            }
            line = stationFileInput.readLine();
        }

        // now iterate over the file a second time adding the edges
        // this is necessary as our graph will not allow the addition
        // of edges if they connect a node that does not yet exist
        line = lineFileInput.readLine();
        while (line != null) {
            st = new StringTokenizer(line);
            try {
                Set<LabelledEdge<Station, String>> edges = parseEdges(st);
                setWeights(edges);
                addEdges(graph, edges);
            } catch (BadFileException e) {
                System.out.println("Bad line in file. Line ignored.");
                e.printStackTrace();
            }
            line = lineFileInput.readLine();
        }
    }

    /**
     * Sets the weights of edges so we can demonstrate the weighted
     * graph search.
     * <p>
     * Green line edges stay at the default of 1, Red line edges are
     * cheaper at 0.2, Orange line edges are more expensive at 2.
     *
     * @param edges for which we want to set the weights
     */
    private void setWeights(Set<LabelledEdge<Station, String>> edges) {
        for (LabelledEdge<Station, String> edge :
                edges) {
            if (edge.getLabel().contains("Red")) {
                edge.setWeight(0.2);
            } else if (edge.getLabel().contains("Orange")) {
                edge.setWeight(2.0);
            }
        }
    }

    /**
     * Adds a set of edges to a graph
     *
     * @param graph That is to have edges added
     * @param edges That are to be added to the graph
     */
    private void addEdges(LabelledMultiGraph<Station, String> graph, Set<LabelledEdge<Station, String>> edges) {
        for (LabelledEdge<Station, String> edge :
                edges) {
            if (edge.to().getId() != 0) {
                // the graph will allow multiple identical edges so we must check
                if (!graph.containsEdge(edge)) {
                    try {
                        graph.addEdge(edge);
                    } catch (NonExistentNodeException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Adds a station to the given graph and also updates the map from stationID to Station
     *
     * @param graph   To which the station is to be added
     * @param station To be added to the graph
     */
    private void addStation(LabelledMultiGraph<Station, String> graph, Station station) {
        stations.add(station);
        graph.addNode(station);
        stationMap.put(station.getId(), station);
    }

    /**
     * Parses all edges it can from a single, supplied, tokenizer.
     *
     * @param st A tokenizer to use in the parsing
     * @return A set of all the edges it managed to parse from a line of text
     * @throws BadFileException If there are no tokens where tokens are expected
     */
    private Set<LabelledEdge<Station, String>> parseEdges(StringTokenizer st) throws BadFileException {
        // first move the tokenizer over the station part of the line
        Station station = parseStation(st);
        Set<LabelledEdge<Station, String>> edges = new HashSet<>();

        if (!st.hasMoreTokens()) {
            throw new BadFileException("station is on no lines");
        }

        Station thisStation = stationMap.get(station.getId());
        // now iterate over every subway line that goes through this station
        while (st.hasMoreTokens()) {
            String lineName = st.nextToken();

            if (!st.hasMoreTokens()) {
                throw new BadFileException("poorly formatted line info");
            }
            int outboundID = Integer.parseInt(st.nextToken());

            if (!st.hasMoreTokens()) {
                throw new BadFileException("poorly formatted adjacent stations");
            }
            int inboundID = Integer.parseInt(st.nextToken());

            if (outboundID != 0) {
                Station outBoundStation = stationMap.get(outboundID);
                edges.addAll(outboundSpecializations(thisStation, outBoundStation, lineName));
            }
            if (inboundID != 0) {
                Station inboundStation = stationMap.get(inboundID);
                edges.add(LabelledEdge.newInstance(thisStation, inboundStation, generalised(lineName)));
            }
        }
        return edges;
    }

    /**
     * Given a line with a general name like Green, creates a List containing
     * separate edges for each specialization of Green.
     *
     * @param thisStation     the station from which the edges should originate
     * @param outBoundStation the station where the edges should terminate
     * @param lineName        the line name
     * @return a list containing all edges created by specializations of the line name
     */
    private List<LabelledEdge<Station, String>> outboundSpecializations(Station thisStation, Station outBoundStation, String lineName) {
        List<String> labels = specializations(lineName);
        List<LabelledEdge<Station, String>> edges = new ArrayList<>();
        for (String label :
                labels) {
            edges.add(LabelledEdge.newInstance(thisStation, outBoundStation, label));
        }
        return edges;
    }

    /**
     * Return a list of all the specializations of a line name.
     * E.g. for Green would return a list containing GreenB, GreenC etc
     *
     * @param lineName for which to generate specializations
     * @return the list of specializations of a line
     */
    private List<String> specializations(String lineName) {
        if (lineName.equals("Green")) {
            return Arrays.asList("Green", "GreenB", "GreenC", "GreenD", "GreenE");
        } else if (lineName.equals("Red")) {
            return Arrays.asList("Red", "RedA", "RedB");
        }
        return Arrays.asList(lineName);
    }

    /**
     * Given a line name, return the generalized version.
     * <p>
     * I.e. GreenB becomes Green, RedA becomes Red etc
     *
     * @param lineName name of the line to be generalized
     * @return the generalized line name
     */
    private String generalised(String lineName) {
        if (lineName.startsWith("Red") && lineName.length() > 3) {
            return "Red";
        } else if (lineName.startsWith("Green") && lineName.length() > 5) {
            return "Green";
        }
        return lineName;
    }

    /**
     * Parses a single station from a supplied tokenizer.
     * <p>
     * The station ID must be the first token, and the station name must
     * be the second token
     *
     * @param st A tokenizer to use in the parsing
     * @return The station it managed to parse from a line of text
     * @throws BadFileException If there are no tokens where tokens are expected
     */
    private Station parseStation(StringTokenizer st) throws BadFileException {
        // ignore empty lines
        if (!st.hasMoreTokens()) {
            throw new BadFileException("empty line");
        }
        int stationID = Integer.parseInt(st.nextToken());

        if (!st.hasMoreTokens()) {
            throw new BadFileException("no station name");
        }
        String stationName = st.nextToken();

        return new Station(stationName, stationID);
    }

    public List<Station> getStations() {
        return stations;
    }

}