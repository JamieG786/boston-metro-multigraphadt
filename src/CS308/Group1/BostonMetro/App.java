package CS308.Group1.BostonMetro;

import CS308.Group1.Graph.LabelledMultiGraph;
import CS308.Group1.Graph.StickyShortestLabelledEdgePathFinder;

import java.io.IOException;
import java.util.Scanner;

/**
 * This class is the driver class for the application.
 *
 * It starts an instance of the application and tells
 * it to run.
 */
public class App {

    private static final String FILE_NAME = "bostonmetro.txt";

    public static void main(String[] args){
        try(Scanner scanner = new Scanner(System.in)) {

            // Create and populate the graph
            LabelledMultiGraph<Station, String> metro = LabelledMultiGraph.newInstance();
            new MetroMapParser(FILE_NAME).generateGraphFromFile(metro);

            // Create the interface
            CommandLineInterface cli = new CommandLineInterface();

            // run the interface
            cli.run(new MetroRoute(new StickyShortestLabelledEdgePathFinder<>(metro)),
                    new StationSelector(metro.getNodes(), scanner),
                    scanner);

        } catch (IOException e) {
            System.out.println("Could not access file \"" + FILE_NAME + "\"");
            e.printStackTrace();
        }
    }

}
