package CS308.Group1.BostonMetro;

import CS308.Group1.Graph.LabelledEdge;
import CS308.Group1.Graph.LabelledEdgePathFinder;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Used to alter and generate paths given by a provided pathfinder
 * Also allows the data to be exported to a string for the sake of building
 * a console application. One can also use a getter to handle the exported data
 * to display in their own way.
 */
public class MetroRoute {
    private Station origin, destination;
    private List<LabelledEdge<Station, String>> stationsInRoute;
    private LabelledEdgePathFinder<Station, String> pathfinder;

    /**
     * Create a new MetroRoute using the given pathfinder
     *
     * @param pf used to find paths between stations
     */
    public MetroRoute(LabelledEdgePathFinder<Station, String> pf) {
        this.pathfinder = pf;
        updateUsedStations();
    }

    /**
     * Create a new MetroRoute using the given pathfinder and set initial values
     * for the origin and destination before getting the shortest path between
     * them.
     *
     * @param origin the origin station
     * @param destination the destination station
     * @param pathfinder used to find paths between stations
     */
    public MetroRoute(Station origin, Station destination, LabelledEdgePathFinder<Station, String> pathfinder) {
        this.origin = origin;
        this.destination = destination;
        this.pathfinder = pathfinder;
        updateUsedStations();
    }

    /**
     * Update the origin station with the given station
     *
     * @param orig the new origin
     */
    public void newOrigin(Station orig) {
        this.origin = orig;
        updateUsedStations();
    }

    /**
     * Update the destination station with the given station
     *
     * @param dest the new destination
     */
    public void newDestination(Station dest) {
        this.destination = dest;
        updateUsedStations();
    }

    /**
     * This function allows the user to generate the path
     * This is called when the origin and destination are initially set
     * and then whenever the origin and destination are changed.
     */
    private void updateUsedStations() {
        this.stationsInRoute = pathfinder.findPath(origin, destination);
    }

    public List<LabelledEdge<Station, String>> getStations() { return stationsInRoute;  }

    /**
     * Displays the route in the format:
     *      Line: (A Line) | Station: (A Station)
     * for every line station passed on visit.
     * @return output String
     */
    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();

        if (stationsInRoute.size() > 0) {
            String firstLine = stationsInRoute.get(0).getLabel();
            Station firstStation = stationsInRoute.get(0).from();

            output.append("Line: ").append(firstLine).append(" | ");
            output.append("Station: ").append(firstStation.getName()).append("\n");

            for (LabelledEdge<Station, String> e : stationsInRoute) {
                output.append("Line: ").append(e.getLabel()).append(" | ");
                output.append("Station: ").append(e.to().getName()).append("\n");
            }
        } else {
            output.append("No route exists");
        }
        return output.toString();
    }


    /**
     * Gives a condensed form of directions that should take the user
     * from the {@link #origin} to the {@link #destination}.
     *
     * I.e. take the XYZ line from A to B
     *      then
     *      take the ZYX line from B to C
     *
     * @return the human readable directions as a string
     */
    public String condensedDirections(){
        StringBuilder output = new StringBuilder("\n");
        List<String> lines = stationsInRoute.stream().map(LabelledEdge::getLabel).collect(Collectors.toList());
        String currentLine = lines.get(0);
        output.append("from ").append(stationsInRoute.get(0).from().getName())
                .append(" take the ").append(currentLine).append(" line to ");
        for (int i = 0; i < lines.size(); i++) {
            currentLine = lines.get(i);
            // if the line continues
            if(i < lines.size()-1 && lines.get(i+1).equals(currentLine)){
                continue;
            }
            // if the line is going to change next
            if(i == lines.size() - 1){
                output.append(stationsInRoute.get(i).to().getName());
            } else {
                // if this line ends
                output.append(stationsInRoute.get(i).to().getName());
                output.append("\n then take the ");
                output.append(lines.get(i+1));
                output.append(" line to ");
            }
        }
        return output.toString();
    }
}
