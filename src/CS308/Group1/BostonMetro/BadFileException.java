package CS308.Group1.BostonMetro;

/**
 * Thrown when there is a problem reading the
 * text file that describes the metro
 */
public class BadFileException extends Exception {
    public BadFileException(String errorMessage) {
        super(errorMessage);
    }
}
