package CS308.Group1.BostonMetro;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Facilitates the selection of a station from all the available stations provided
 * by a list.
 * <p>
 * This class should externally be used as follows:
 * <p>
 * Station station = stationSelector.getUserSelection("some-description");
 * <p>
 * The class will first ask the user to search for the station, and then
 * choose one from a small list.
 * <p>
 * Instead of searching a station name the user may type "list" to view a list
 * of all the stations (in alphabetical order) or "exit" to return null.
 * Users of this class should ensure the application exits if it returns null.
 */
public class StationSelector {

    // the number of columns over which the full list of stations
    // is to be split
    private static final int NUM_COLUMNS = 5;
    public static final String MENU_SELECTION_ERROR = "Invalid entry. Please either enter a valid option or 0 to search again.";
    public static final String MENU_PROMPT = "Pick a number from the search results above: ";
    public static final String ADDITIONAL_SEARCH_INSTRUCTIONS = "\nType \"list\" to view all stations or \"exit\" to quit.";
    public static final String NO_RESULTS = "No results found. Please try another search.";
    public static final String SEARCH_INSTRUCTIONS = "\nChoose a station by searching for it by full or partial name.";

    private List<Station> stations;
    private Scanner scanner;
    private final int NUM_ROWS;

    /**
     * Creates a station selector for the supplied list of stations.
     *
     * @param stations from which we will be asking the user to select
     * @param scanner  to take input
     */
    public StationSelector(List<Station> stations, Scanner scanner) {
        this.scanner = scanner;
        this.stations = stations;
        Collections.sort(this.stations);
        this.NUM_ROWS = (int) Math.ceil((float) stations.size() / NUM_COLUMNS);
    }

    /**
     * Returns a station, selected by the user, from {@link #stations}.
     * <p>
     * If the user indicates they wish to quit instead, returns null.
     *
     * @param description of the station being selected, e.g. "origin" or "destination"
     * @return a station selected by the user, or null if the user wishes to quit
     */
    public Station getUserSelection(String description) {
        List<Station> candidates;
        int selection;
        do {
            // search to get a list of candidate stations
            do {
                candidates = userSearch(description);
                if (candidates == null) {
                    // returning null should exit the application
                    return null;
                }
            } while (candidates.size() == 0);
            printMenu(candidates);
            // ask the user to choose one of the candidates
            selection = getSelection(candidates.size());
        } while (selection == 0);
        return candidates.get(selection - 1);
    }

    /**
     * Returns an integer representing a choice in a menu.
     * <p>
     * E.g. for some menu of the form
     * 1) someChoice
     * 2) someOtherChoice
     * 3) yetAnotherChoice
     * <p>
     * returns some integer, chosen by the user, between 0 and 3
     * where 1-3 represent a chosen menu option and 0 represents
     * none of the menu options.
     *
     * @param menuSize how many items are int the menu
     * @return the number of the chosen menu item or 0 to indicate no choice
     */
    private int getSelection(int menuSize) {
        int selection;
        do {
            System.out.print(MENU_PROMPT);
            try {
                selection = scanner.nextInt();
            } catch (InputMismatchException e) {
                selection = -1;
            }
            scanner.nextLine();
            if (selection < 0 || selection > menuSize) {
                System.out.println(MENU_SELECTION_ERROR);
            }
        } while (selection < 0 || selection > menuSize);
        return selection;
    }

    /**
     * Searches for all stations that match a user query, the user may also
     * type "list" to display all possible choices and "exit" if they no
     * longer wish to search.
     *
     * @param description of the station the user should searching for e.g. "origin" or "destination"
     * @return a list of stations that hopefully matches the users search
     */
    private List<Station> userSearch(String description) {
        // get the users search query
        System.out.println(ADDITIONAL_SEARCH_INSTRUCTIONS);
        System.out.print("Search for " + description + ": ");
        String userQuery = scanner.nextLine();
        // deal with any special instructions from the user
        if (userQuery.trim().toLowerCase().equals("list")) {
            printAllStations();
            return new ArrayList<>();
        } else if (userQuery.trim().toLowerCase().equals("exit")) {
            return null;
        }

        List<Station> results = searchStations(userQuery);

        if (results.size() == 0) {
            System.out.println(NO_RESULTS);
        }
        return results;
    }

    /**
     * Returns a list of all matching stations from {@link #stations} given a query string.
     * <p>
     * A station that matches is one whose name contains at least one of the words contained
     * in the user search string.
     *
     * @param query the search string
     * @return A list of all matching stations from {@link #stations }
     */
    private List<Station> searchStations(String query) {
        String[] tokens = query.split("\\s+");

        return stations.stream()
                .filter(station ->
                        Arrays.stream(tokens)
                                .anyMatch(token -> !token.equals("") &&
                                        station.getName().toLowerCase().contains(token.toLowerCase())))
                .collect(Collectors.toList());
    }

    /**
     * Prints a list of station names, in order, preceded by an incrementing number
     * <p>
     * E.g. 1) astation
     * 2) another
     *
     * @param candidates the list of stations whose names we would like to print
     */
    private void printMenu(List<Station> candidates) {
        for (int i = 0; i < candidates.size(); i++) {
            System.out.println("\t" + (i + 1) + ") " + candidates.get(i).getName());
        }
    }

    /**
     * Prints a user-friendly formatting of all station names in {@link #stations}.
     * <p>
     * The stations are printed in columns, the number of which is determined by
     * {@link #NUM_COLUMNS}.
     */
    private void printAllStations() {
        // note the longest name so we can format nicely
        int maxStationNameSize = stations.stream()
                .map(s -> s.getName().length())
                .max(Integer::compareTo)
                .orElse(0);

        List<List<Station>> table = sortStationsIntoTable();

        System.out.println();
        for (List<Station> row : table) {
            for (Station station : row) {
                System.out.printf("%-" + (maxStationNameSize + 1) + "s ", station.getName());
            }
            // every row on a newline
            System.out.println();
        }
        System.out.println(SEARCH_INSTRUCTIONS);
    }

    /**
     * Returns a list of list of stations where each inner list
     * represent a row in a table with {@link #NUM_COLUMNS} columns
     * and {@link #NUM_ROWS} rows.
     *
     * @return a list of list of stations, where each inner list represents
     * a row in a table
     */
    private List<List<Station>> sortStationsIntoTable() {
        // create and empty list of rows
        List<List<Station>> table = new ArrayList<>();
        for (int i = 0; i < NUM_ROWS; i++) {
            table.add(new ArrayList<>());
        }
        // and sort the stations into the required row
        for (int i = 0; i < stations.size(); i++) {
            table.get(i % NUM_ROWS).add(stations.get(i));
        }
        return table;
    }
}
